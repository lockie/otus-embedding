#include <string.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <microhttpd.h>

#include "lib.h"


static int http_url(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    const connection_data* cd = lua_topointer(lua, 1);
    lua_pushstring(lua, cd->url);
    return 1;
}

static int http_method(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    const connection_data* cd = lua_topointer(lua, 1);
    lua_pushstring(lua, cd->method);
    return 1;
}

static int http_version(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    const connection_data* cd = lua_topointer(lua, 1);
    lua_pushstring(lua, cd->version);
    return 1;
}

static int http_header(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    luaL_checktype(lua, 2, LUA_TSTRING);

    const connection_data* cd = lua_topointer(lua, 1);
    size_t key_size = 0;
    const char* key = lua_tolstring(lua, 2, &key_size);

    const char* value;
    size_t value_size;
    if(MHD_lookup_connection_value_n(
           cd->connection, MHD_HEADER_KIND,
           key, key_size, &value, &value_size) != MHD_YES)
        lua_pushnil(lua);
    else
        lua_pushlstring(lua, value, value_size);
    return 1;
}

static int http_cookie(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    luaL_checktype(lua, 2, LUA_TSTRING);

    const connection_data* cd = lua_topointer(lua, 1);
    size_t key_size = 0;
    const char* key = lua_tolstring(lua, 2, &key_size);

    const char* value;
    size_t value_size;
    if(MHD_lookup_connection_value_n(
           cd->connection, MHD_COOKIE_KIND,
           key, key_size, &value, &value_size) != MHD_YES)
        lua_pushnil(lua);
    else
        lua_pushlstring(lua, value, value_size);
    return 1;

}

static int http_data(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    luaL_checktype(lua, 2, LUA_TSTRING);

    const connection_data* cd = lua_topointer(lua, 1);
    if(strcmp(cd->method, MHD_HTTP_METHOD_GET) == 0)
    {
        size_t key_size = 0;
        const char* key = lua_tolstring(lua, 2, &key_size);

        const char* value;
        size_t value_size;
        if(MHD_lookup_connection_value_n(
               cd->connection, MHD_GET_ARGUMENT_KIND,
               key, key_size, &value, &value_size) != MHD_YES)
            lua_pushnil(lua);
        else
            lua_pushlstring(lua, value, value_size);
    }
    else if(strcmp(cd->method, MHD_HTTP_METHOD_POST) == 0)
    {
        fprintf(stderr, "WARNING: POST data not supported\n");
        lua_pushnil(lua);
    }
    else
    {
        lua_pushnil(lua);
    }
    return 1;
}

static enum MHD_Result iterator(void* cls, enum MHD_ValueKind,
                                const char* key, const char* value)
{
    lua_State* lua = cls;
    lua_pushstring(lua, key);
    lua_pushstring(lua, value);
    lua_rawset(lua, -3);
    return MHD_YES;
}

static int http_headers(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    const connection_data* cd = lua_topointer(lua, 1);
    lua_newtable(lua);
    MHD_get_connection_values(cd->connection, MHD_HEADER_KIND, iterator, lua);
    return 1;
}

static int http_cookies(lua_State* lua)
{
    luaL_checktype(lua, 1, LUA_TLIGHTUSERDATA);
    const connection_data* cd = lua_topointer(lua, 1);
    lua_newtable(lua);
    MHD_get_connection_values(cd->connection, MHD_COOKIE_KIND, iterator, lua);
    return 1;
}

static const struct luaL_Reg httplib[] = {
    {"url", http_url},
    {"method", http_method},
    {"version", http_version},
    {"headers", http_headers},
    {"header", http_header},
    {"cookies", http_cookies},
    {"cookie", http_cookie},
    {"data", http_data},
    {NULL, NULL}  // sentinel
};

void register_httplib(lua_State* lua)
{
    luaL_openlib(lua, "http", httplib, 0);
}
