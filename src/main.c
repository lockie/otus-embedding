#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#include <lua.h>
#include <lualib.h>
#include <lauxlib.h>

#include <microhttpd.h>

#include "lib.h"


void dumpstack(lua_State* L)
{
    int top = lua_gettop(L);
    for(int i = 1; i <= top; i++)
    {
        printf("%d\t%s\t", i, luaL_typename(L, i));
        switch (lua_type(L, i))
        {
        case LUA_TNUMBER:
            printf("%g\n", lua_tonumber(L, i));
            break;
        case LUA_TSTRING:
            printf("%s\n", lua_tostring(L, i));
            break;
        case LUA_TBOOLEAN:
            printf("%s\n", (lua_toboolean(L, i) ? "true" : "false"));
            break;
        case LUA_TNIL:
            printf("%s\n", "nil");
            break;
        default:
            printf("%p\n",lua_topointer(L,i));
            break;
        }
    }
}

#define PATH_FORMAT ".%s.lua"
#define ERROR_OOM "Out of memory"

static enum MHD_Result
handle_connection(void* data, struct MHD_Connection* connection,
                  const char* url, const char* method,
                  const char* version, const char* /*upload_data*/,
                  size_t* /*upload_data_size*/, void** /*req_cls*/)
{
    struct MHD_Response* resp = NULL;
    enum MHD_Result res = MHD_YES;
    unsigned int status_code = MHD_HTTP_OK;
    lua_State* lua = data;

    // build script file path
    size_t path_size = snprintf(NULL, 0, PATH_FORMAT, url) + 1;
    char* path = malloc(path_size);
    if(!path)
    {
        resp = MHD_create_response_from_buffer(
            sizeof(ERROR_OOM), ERROR_OOM,
            MHD_RESPMEM_PERSISTENT);
        status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        goto response;
    }
    snprintf(path, path_size, PATH_FORMAT, url);

    // load script file
    if(luaL_loadfile(lua, path) != 0)
    {
        resp = MHD_create_response_from_buffer(
            lua_strlen(lua, -1), (void*)lua_tostring(lua, -1),
            MHD_RESPMEM_MUST_COPY);
        status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        goto response;
    }

    // run script
    if(lua_pcall(lua, 0, 1, 0) != 0)
    {
        resp = MHD_create_response_from_buffer(
            lua_strlen(lua, -1), (void*)lua_tostring(lua, -1),
            MHD_RESPMEM_MUST_COPY);
        status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        goto response;
    }

    // execute resulting function
    connection_data cd = {
        .connection = connection,
        .url = url,
        .method = method,
        .version = version
    };
    lua_pushlightuserdata(lua, &cd);
    if(lua_pcall(lua, 1, 3, 0) != 0)
    {
        resp = MHD_create_response_from_buffer(
            lua_strlen(lua, -1), (void*)lua_tostring(lua, -1),
            MHD_RESPMEM_MUST_COPY);
        status_code = MHD_HTTP_INTERNAL_SERVER_ERROR;
        goto response;
    }

    // figure out response body
    resp = MHD_create_response_from_buffer(
        lua_strlen(lua, -3), (void*)lua_tostring(lua, -3),
        MHD_RESPMEM_MUST_COPY);

    // figure out response status code
    status_code = (int)lua_tonumber(lua, -2);
    if(!status_code)
        status_code = MHD_HTTP_OK;

    // figure out response headers
    int h = lua_gettop(lua);
    if(lua_istable(lua, h))
    {
        lua_pushnil(lua);
        while(lua_next(lua, h) != 0)
        {
            MHD_add_response_header(
                resp,
                lua_tostring(lua, -2),
                lua_tostring(lua, -1));
            lua_pop(lua, 1);
        }
    }

response:
    res = MHD_queue_response(connection, status_code, resp);
    MHD_destroy_response(resp);
    lua_pop(lua, lua_gettop(lua));
    free(path);
    return res;
}

int main(int argc, char** argv)
{
    if(argc != 3)
    {
        printf("USAGE: %s <port> <rootdir>\n", argv[0]);
        return EXIT_FAILURE;
    }

    short int portnum = atoi(argv[1]);
    if(!portnum)
    {
        printf("Incorrect port: %s\n", argv[1]);
        printf("USAGE: %s <port>\n", argv[0]);
        return EXIT_FAILURE;
    }

    if(chdir(argv[2]) != 0)
    {
        perror("Failed to set root");
        return EXIT_FAILURE;
    }

    lua_State* lua = luaL_newstate();
    if(!lua)
    {
        perror("Failed to initialize Lua");
        return EXIT_FAILURE;
    }
    luaL_openlibs(lua);
    register_httplib(lua);

    struct MHD_Daemon* daemon = MHD_start_daemon(
        MHD_USE_AUTO | MHD_USE_INTERNAL_POLLING_THREAD | MHD_USE_ERROR_LOG,
        portnum, NULL, NULL, &handle_connection, lua, MHD_OPTION_END);

    if(!daemon)
    {
        perror("Failed to start server");
        lua_close(lua);
        return EXIT_FAILURE;
    }

    printf("Started server on port %d\n", portnum);
    (void)getchar();

    MHD_stop_daemon(daemon);
    lua_close(lua);

    return EXIT_SUCCESS;
}
