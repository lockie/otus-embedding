#ifndef LIB_H_
#define LIB_H_


#include <lua.h>

typedef struct {
    struct MHD_Connection* connection;
    const char* url;
    const char* method;
    const char* version;
} connection_data;

void register_httplib(lua_State* lua);


#endif  // LIB_H_
