
local tags = require "tags"
local html, head, body, h1, p, br = tags(
   "html", "head", "body", "h1", "p", "br")

return function()
   return tostring(
      html(
         body(
            h1 "Hello world!",
            p "This is simple HTML page generated by Lua.",
            br,
            p "It is awesome!"
         )
      )
   )
end
