
local function table_to_string(t)
   local result = ""
   for name, value in pairs(t) do
      result = result .. name .. "=" .. value .. " "
   end
   return result
end

return function (request)
   return "URL: " .. http.url(request)
      .. "\nmethod: " .. http.method(request)
      .. "\nversion: " .. http.version(request)
      .. "\nquery: " .. (http.data(request, "query") or "none")
      .. "\nlanguage: " .. (http.header(request, "accept-language") or "none")
      .. "\ncookies: " .. table_to_string(http.cookies(request))
      .. "\nheaders: " .. table_to_string(http.headers(request))
end
