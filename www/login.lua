
local tags = require "tags"
local html, head, meta, title, link, script, body, main, h1, div, p, img, a, form, input, label, button = tags(
   "html", "head", "meta", "title", "link", "script", "body", "main", "h1", "div", "p", "img", "a", "form", "input", "label", "button")

local function wrap_content(content, t)
   return tostring(
      html {lang="en"} (
         head (
            meta {charset="utf-8"},
            meta {name="viewport", content="width=device-width, initial-scale=1"},
            title(t),
            link {
               href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css",
               rel="stylesheet"
            }
         ),
         body {class="text-center"} (
            content
         )
      )
   )
end

return function(request)
   if http.cookie(request, "username") == "admin" then
      return wrap_content(h1 "Secret data", "Secret data")
   else
      if http.data(request, "email") == "admin@example.com" and
         http.data(request, "password") == "password" then
         return "", 301, {["Set-Cookie"] = "username=admin", Location = "/login"}
      end
      return wrap_content(
         main {class="w-100 m-auto", style="max-width: 330px;"} (
            form {method="get", action="/login"} (
               img {class="mb-4",
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Lua-Logo.svg/240px-Lua-Logo.svg.png",
                    width="72", height="72"},
               h1 {class="h3 mb-3 fw-normal"} "You need to log in to continue",
               div {class="form-floating"} (
                  input {type="email", class="form-control", id="email", name="email"},
                  label {["for"]="email"} "Email address"
               ),
               div {class="form-floating"} (
                  input {type="password", class="form-control", id="password", name="password"},
                  label {["for"]="password"} "Password"
               ),
               button {class="w-100 btn btn-lg btn-primary", type="submit"} "Log in"
            )
         )
      )
   end
end
