
local tags = require "tags"
local html, head, meta, title, link, script, body, h1, div, p, img, a = tags(
   "html", "head", "meta", "title", "link", "script", "body", "h1", "div", "p", "img", "a")

return function()
   return tostring(
      html {lang="en"} (
         head (
            meta {charset="utf-8"},
            meta {name="viewport", content="width=device-width, initial-scale=1"},
            title "Lua landing page",
            link {
               href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/css/bootstrap.min.css",
               rel="stylesheet"
            }
         ),
         body(
            div {class="px-4 py-5 my-5 text-center"} (
               img {class="d-block mx-auto mb-4",
                    src="https://upload.wikimedia.org/wikipedia/commons/thumb/c/cf/Lua-Logo.svg/240px-Lua-Logo.svg.png",
                    width="72", height="72"},
               h1 {class="display-5 fw-bold"} "The Programming Language Lua",
               div {class="col-lg-6 mx-auto"} (
                  p {class="lead mb-4"} "Lua is a powerful, efficient, lightweight, embeddable scripting language. It supports procedural programming, object-oriented programming, functional programming, data-driven programming, and data description.",
                  div {class="d-grid gap-2 d-sm-flex justify-content-sm-center"} (
                     a {href="https://lua.org/download.html",
                        role="button",
                        class="btn btn-primary btn-lg px-4 gap-3"} "Download",
                     a {href="https://lua.org/docs.html",
                        role="button",
                        class="btn btn-outline-secondary btn-lg px-4"} "Learn more"
                  )
               )
            ),
            script {src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.0-beta1/dist/js/bootstrap.bundle.min.js"}
         )
      )
   )
end
